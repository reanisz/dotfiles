#!/bin/sh
rm $HOME/.vimrc
rm $HOME/.vimrc.plugin
rm $HOME/.vimrc.local
rm $HOME/.vim
rm $HOME/.zshrc
rm $HOME/.zshrc.local
rm $HOME/.tmux.conf
